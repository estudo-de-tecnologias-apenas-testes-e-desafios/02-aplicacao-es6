import api from "./api";
import "babel-polyfill";

class App {
  constructor() {
    this.repositories = [
    {
      image_url: "https://cdn.myanimelist.net/images/anime/7/76014.jpg",
      title: "Haikyuu!!",
      score: "8.59",
      url: "https://myanimelist.net/anime/20583/Haikyuu"
    }];
    
    this.inputEl = document.querySelector("input[name=repository]");    
    this.formEl = document.getElementById("repo-form");
    this.listEl = document.getElementById("repo-list");

    this.render();
    this.registerHandlers();
  }

  registerHandlers() {
    this.formEl.onsubmit = event => this.addRepository(event);
  }

  setLoading(loading = true) {
    if (loading) {
      let loadingEl = document.createElement("p");
      loadingEl.setAttribute("id", "carregando");
      loadingEl.appendChild(document.createTextNode("Carregando"));
      this.formEl.appendChild(loadingEl);
    } else {
      document.querySelector("#carregando").remove();
    }
  }

  async addRepository(event) {
    event.preventDefault();

    const id = this.inputEl.value;

    if(id.length === 0) return;

    this.setLoading();

    try{
      const request = await api.get(`/${id}`);

      const {image_url, title, score, url} = request.data;

      this.repositories.push({
        image_url,
        title,
        score,
        url
      });

      this.inputEl.value = '';
    } catch (err) {
      alert("Anime não existe");
    }

    this.setLoading(false);

    this.render();
  }

  render() {
    this.listEl.innerHTML = '';

    this.repositories.forEach(repo => {
      let imageEl = document.createElement("img");
      imageEl.setAttribute("src", repo.image_url);

      let tituloEl = document.createElement("strong");
      tituloEl.appendChild(document.createTextNode(repo.title));

      let scoreEl = document.createElement("p");
      scoreEl.appendChild(document.createTextNode(repo.score));

      let linkEl = document.createElement("a");
      linkEl.appendChild(document.createTextNode("Acessar"));
      linkEl.setAttribute("href", repo.url);

      let liEl = document.createElement("li");
      liEl.appendChild(imageEl);
      liEl.appendChild(tituloEl);
      liEl.appendChild(scoreEl);
      liEl.appendChild(linkEl);

      this.listEl.appendChild(liEl);
    });
  }
}

new App();